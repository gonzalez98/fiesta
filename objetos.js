function empleados(){

    //------------------------------------------------------------------------------
    let lienzo = document.getElementById('draw');
    let ctx = lienzo.getContext("2d");



    ctx.beginPath();
    ctx.fillStyle = "#FF0000"
    ctx.arc(300,200,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(30,570,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(90,570,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(400,24,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(550,350,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#FF0000"
    ctx.arc(685,40,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(685,40,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#FFFFFF"
    ctx.font = "20px Georgia";
    ctx.fillText("DJ",670,45,50);
    ctx.closePath();
    ctx.fill();
    //------------------------------------------------------------------------------

    let emp1 = new Empleados("Carlos ","DJ");
    let emp2 = new Empleados("Juan ", "Bartender");
    let emp3 = new Empleados("Camila ", "Mesera");
    let emp4 = new Empleados("Juana ", "Mesera");
    let emp5 = new Empleados("Jorge ", "Seguridad");
    let emp6 = new Empleados("Kevin ", "Seguridad");

    e1= " "+emp1.nombre + " " + "("+emp1.funcion+")";
    e2= " "+emp2.nombre + " " + "("+emp2.funcion+")";
    e3= " "+emp3.nombre + " " + "("+emp3.funcion+")";
    e4= " "+emp4.nombre + " " + "("+emp4.funcion+")";
    e5= " "+emp5.nombre + " " + "("+emp5.funcion+")";
    e6= " "+emp6.nombre + " " + "("+emp6.funcion+")";

    let lista1 = [e1, e2, e3, e4, e5, e6];


    document.getElementById('emp').innerHTML= lista1;
    emp3.atender();
    emp2.servir();
    emp4.atender();
    emp1.musica();
    emp5.seguridad();
    emp6.seguridad();

}



function invitados(){

    //-----------------------------------------------------------------------
    let lienzo = document.getElementById('draw');
    let ctx = lienzo.getContext("2d");

    ctx.beginPath();
    ctx.fillStyle = "#F700FF"
    ctx.arc(295,560,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(150,393,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(640,185,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(410,300,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(470,425,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(580,450,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(640,510,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#0017FF"
    ctx.arc(295,435,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(30,393,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(85,185,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(400,100,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(570,250,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(710,250,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(395,337,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.fill();
    //--------------------------------------------------------------------------------

    let inv1 = new Invitados("Kerly ", "652581", true, true, true, true, true, true, true);
    let inv2 = new Invitados("Monica ", "657536", true, true, true, true, true, true, false);
    let inv3 = new Invitados("Dana ", "657895", true, true, false, true, true, false, true);
    let inv4 = new Invitados("Ana ", "651234", true, true, true, true, true, true, true);
    let inv5 = new Invitados("Karen ", "652482", true, true, false, true, true, true, true);
    let inv6 = new Invitados("Joanna ", "652367", true, true, true, true, true, true, true);
    let inv7 = new Invitados("Juan ", "652758", true, true, false, true, true, true, true);
    let inv8 = new Invitados("Camilo ", "668247", true, true, true, true, true, true, true);
    let inv9 = new Invitados("Julian ", "652539", true, true, true, true, true, true, true);
    let inv10 = new Invitados("Cristian ", "648523", true, true, true, true, true, true, true);
    let inv11 = new Invitados("Diego ", "652258", true, true, true, true, true, true, true);
    let inv12 = new Invitados("Jhon ", "652736", true, true, true, true, true, true, true);
    let inv13 = new Invitados("Kevin ", "652714", true, true, false, false, false, false, true);
    let inv14 = new Invitados("Brayan ", "652936", true, true, true, true, true, true, true);
    
    inv1.baile();
    inv7.beber();
    inv12.comida();
    inv5.jugar();
    inv3.conversacion();
    inv9.conversacion();
    inv2.beber();
    inv4.jugar();
    inv14.baile();
    inv6.conversacion();
    inv8.baile();
    inv10.beber();
    inv11.comida();
    inv13.baile();







    e1 = " "+ inv1.nombre +" "+"Cedula:"+inv1.cedula;
    e2 = " "+ inv2.nombre +" "+"Cedula:"+inv2.cedula;
    e3 = " "+ inv3.nombre +" "+"Cedula:"+inv3.cedula;
    e4 = " "+ inv4.nombre +" "+"Cedula:"+inv4.cedula;
    e5 = " "+ inv5.nombre +" "+"Cedula:"+inv5.cedula;
    e6 = " "+ inv6.nombre +" "+"Cedula:"+inv6.cedula;
    e7 = " "+ inv7.nombre +" "+"Cedula:"+inv7.cedula;
    e8 = " "+ inv8.nombre +" "+"Cedula:"+inv8.cedula;
    e9 = " "+ inv9.nombre +" "+"Cedula:"+inv9.cedula;
    e10 = " " +inv10.nombre +" "+"Cedula:"+inv10.cedula;
    e11 = " " +inv11.nombre +" "+"Cedula:"+inv11.cedula;
    e12 = " " +inv12.nombre +" "+"Cedula:"+inv12.cedula;
    e13 = " " +inv13.nombre +" "+"Cedula:"+inv13.cedula;
    e14 = " " +inv14.nombre +" "+"Cedula:"+inv14.cedula;

    let lista2 = [e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14];

    document.getElementById('inv').innerHTML= lista2;

}

