function dibujo(){

    let lienzo = document.getElementById('draw');
    let ctx = lienzo.getContext("2d");

    ctx.beginPath();
    ctx.fillStyle = "#FFFFFF" //Mesas
    ctx.fillRect(250, 45, 250, 30);
    ctx.fillRect(500,-175, 30, 250);
    ctx.fillRect(50, 350, 85, 95);
    ctx.fillRect(600, 200, 85, 95);
    ctx.fillRect(600, 400, 85, 95);
    ctx.fillRect(430, 450, 85, 95);
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#1B67A3"  // PingPong
    ctx.fillRect(250, 450, 85, 95);
    ctx.closePath();
    ctx.fill();


    ctx.beginPath();
    ctx.fillStyle = "#226624" //Bolirana
    ctx.fillRect(2, 170, 60, 30);
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#36FF00"  
    ctx.fillRect(-1, -1, 160, 60);
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#000000"
    ctx.font = "25px Georgia";
    ctx.fillText("BAÑO",40,40,50);
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#36FF00"
    ctx.arc(750,0,150,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#000000"
    ctx.fillRect(650, 60, 75, 30);
    ctx.closePath();
    ctx.fill();


}


    